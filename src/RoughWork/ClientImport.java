package RoughWork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;
import com.selenium.commonfiles.base.TestBase;

public class ClientImport extends TestBase{

		//This test method declares that its data should be supplied by the Data Provider
		// "getdata" is the function name which is passing the data
	    // Number of columns should match the number of input pameters
	
		public WebDriver driver;
		public WebDriverWait wait;
		String appURL = "https://206.142.240.56/stingray/web/login.jsp";
		FileInputStream fis;
		protected final static String workDir = System.getProperty("user.dir");
		public static ExtentReports report = new ExtentReports("D:\\AJG\\ClientImportScript\\ExecutionReport\\Execution_report.html",NetworkMode.OFFLINE);
		public static ExtentTest logger;
		public String Scenario=null;
		Throwable t;
		public FileWriter writer;
		public String workDirResult = "D:/";
		
		@BeforeTest
		public void openBrowser() {
			Scenario = this.getClass().getSimpleName();
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\com\\selenium\\configuration\\Add-ons\\ChromeDriverServer_Win\\chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver, 5);
			
			driver.navigate().to(appURL);
			driver.findElement(By.xpath("/html/body/div[1]/form/table/tbody/tr[1]/td[2]/input")).sendKeys("Paul Starling");
			driver.findElement(By.xpath("/html/body/div[1]/form/table/tbody/tr[2]/td[2]/input")).sendKeys("pas5wordA");
			//wait for element to be visible and perform click
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='lk2']/div/a")));
			driver.findElement(By.xpath("//*[@id='lk2']/div/a")).click();
		}
		
		
		@AfterTest
		public void closeBrowser() throws IOException {
			
			fis.close();
			driver.close();
			//driver.quit();
			
		}
		
		@Test(dataProvider="getData")
		public void setData(String legacy_number , String Name, String tradingName , String Address_Line_1 , String Address_Line_2 , String Address_Line_3 , String Town , String County , String Postcode) throws InterruptedException, IOException
		{
			
			try{
				logger = report.startTest("Legacy Client Number : "+legacy_number);
				driver.findElement(By.xpath("//*[@id='nb-menu']/ul/li[4]/a")).click();
				driver.findElement(By.xpath("//*[@id='add-client']")).click();
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[2]/td[2]/input")).sendKeys(Name.trim());
				//driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[3]/td[2]/input")).sendKeys(Trading_Name.trim());
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[5]/td[2]/input")).sendKeys(Address_Line_1.trim());
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[6]/td[2]/input")).sendKeys(Address_Line_2.trim());
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[7]/td[2]/input")).sendKeys(Address_Line_3.trim());
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[8]/td[2]/input")).sendKeys(Town.trim());
				driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[9]/td[2]/input")).sendKeys(County.trim());
				if(validatePostCode(Postcode)){
					driver.findElement(By.xpath("//*[@id='f5']/tbody/tr[11]/td[2]/input")).sendKeys(Postcode.trim());
				}
				driver.findElement(By.xpath("//*[@id='save-client']")).click();
				
				if(driver.findElement(By.xpath("//*[@id='edit-client']")).isDisplayed()){
					driver.findElement(By.xpath("//*[@id='clt-nxt']")).click();
					driver.findElement(By.xpath("//*[@id='edit-client-det']")).click();
					driver.findElement(By.xpath("//*[@id='f4.1']/tbody/tr[3]/td[2]/input")).sendKeys(legacy_number);
					driver.findElement(By.xpath("//*[@id='save-client-det']")).click();
					logger.log(LogStatus.PASS,"Client <b>"+Name+"</b> is added succesfully with Legacy Client Number : <b>"+legacy_number+"</b>");
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode,"Client Added","");
				}
				
			}
			catch(Exception e){
				//logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully with Legacy Client Number : <b>"+legacy_number+"</b>");
				if(Name.equalsIgnoreCase("") || Name==null || Name.isEmpty()){
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode,"Client Not Added" , "Company Name is empty which is mandatory field.");
					logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully because Comapny Name is empty which is mandatory.");
				}else if(Address_Line_1.equalsIgnoreCase("") || Address_Line_1==null || Address_Line_1.isEmpty()){
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode,"Client Not Added" , "Address Line 1 is empty which is mandatory field.");
					logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully because Address line is empty which is mandatory.");
				}else if(Postcode.equalsIgnoreCase("") || Postcode==null || Postcode.isEmpty()){
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode,"Client Not Added" , "Postcode is empty which is mandatory field.");
					logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully because Postcode is empty which is mandatory.");
				}else if(!(validatePostCode(Postcode))){
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode,"Client Not Added" , "Postcode is not valid.");
					logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully because Postcode pattern is not valid.");
				}else{
					writeToTextFile(legacy_number,Name,Address_Line_1,Address_Line_2,Address_Line_3,Town,County,Postcode, "Client Not Added", "Client not added due to some system/application/object issue." );
					logger.log(LogStatus.FAIL,"Client <b>"+Name+"</b> is not added succesfully due to some system/application/object issue.");
				}
			}
			report.endTest(logger);
			report.flush();
		}

		@DataProvider
		public Object[][] getData() throws IOException{
		
		//Rows - Number of times your test has to be repeated.
		//Columns - Number of parameters in test data.
		int count = 0;
		File f = new File(workDirResult);
		File[] listOfFiles = f.listFiles();
		int size = listOfFiles.length;
		System.out.println(size);
		
		for(int i=0;i<size;i++){
			if (listOfFiles[i].isFile()) {
				if(listOfFiles[i].getName().equalsIgnoreCase("TestResults.csv")){
					count++;
					break;
				}
			}
		}
		
		switch (count) {
		case 0:
			
			writer = new FileWriter(workDirResult+"TestResults.csv", true);
			writer.write("Legacy Client number,Name,Address Line 1,Address Line 2,Address Line 3,Primary City,County,Primary Postcode,Result,Remarks\r\n");
	        writer.close();
	        break;

		default:
			break;
		}
		
		fis = new FileInputStream(new File("D:/AJG/ClientImportScript/ClientList.xlsx"));
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sh = wb.getSheetAt(0);
		XSSFRow row = null;
		int totalNoOfRows = sh.getPhysicalNumberOfRows();
		int cell1 = sh.getRow(1).getLastCellNum();
		Object[][] data = new Object[totalNoOfRows-1][cell1];;
		
		for(int i = 0;i<totalNoOfRows-1;i++){
			
			row = sh.getRow(i+1);
			int cell = row.getLastCellNum();
			
			
			for (int j = 0; j <cell; j++) {
				
				XSSFCell c = row.getCell(j);
				
				if(c==null){
					data[i][j] = "";
					continue;
				}
				
				switch (row.getCell(j).getCellType()) {
				case 0:
					data[i][j] = NumberToTextConverter.toText(row.getCell(j).getNumericCellValue());
					break;
				case 3:
					data[i][j] = "";
					break;
				default:
					data[i][j] = row.getCell(j).getStringCellValue();
					break;
				}
		    }
		}
		
		return data;
		
		}
		
		public void writeToTextFile(String legacy_number , String Name,String Address_Line_1 , String Address_Line_2 , String Address_Line_3 , String Town , String County , String Postcode ,String result , String remark) {
	        
	         try {
	               
	             writer = new FileWriter(workDirResult+"TestResults.csv", true);
	             writer.write(legacy_number+","+Name+","+Address_Line_1+","+Address_Line_2+","+Address_Line_3+","+Town+","+County+","+Postcode+","+result+","+remark+"\r\n");
	             writer.close();
	           
	         } catch (IOException e) {
	             e.printStackTrace();
	         }

	     }
		
		public boolean validatePostCode(String postCode){
	    	   
	    	   boolean result=true;
	    	   try{
	    		   //ME16 0CD
	    		   result = Pattern.matches("[a-zA-Z]{2}[0-9]{2} [0-9]{1}[a-zA-Z]{2}", postCode);
	    	   }catch(Throwable t){
	    		   System.out.println("Error while validating Post Code"+t.getMessage());
	    		   result=false;
	    	   }   
	    	   
	    	   return result;
	       }
	
}
