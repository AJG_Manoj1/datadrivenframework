package RoughWork;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class UKTime {

	public static void main(String[] args) {


		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		f.setTimeZone(TimeZone.getTimeZone("Europe/London"));
		Calendar cal = GregorianCalendar.getInstance();
		cal.add(Calendar.HOUR, 0);
		System.out.println(f.format(cal.getTime()));
		
		SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		f1.setTimeZone(TimeZone.getTimeZone("IST"));
		Calendar cal1 = GregorianCalendar.getInstance();
		cal1.add(Calendar.MINUTE, -330);
		System.out.println(f1.format(cal1.getTime()));
		

	}

}
