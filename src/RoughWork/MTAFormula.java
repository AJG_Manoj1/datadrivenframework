package RoughWork;

import java.text.DecimalFormat;

import javax.script.ScriptException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MTAFormula {
	
	
	/*duration is number of days a policy will be on cover, we enter in MTA
	 * initial_prem is the premium amt generated in NB or we can say previous premium
	 * new_prem is the premium amt generated in MTA or we can say revised premium
	 * days_before_endorsement is effective Date of MTA - policy start date
	 * days_remaining is policy end date- effective date of MTA, we can get it from page also*/
	
	public static double checkprem(int duration,double initial_prem, double new_prem,int days_before_endorsement, int days_remaining)
	{
		DecimalFormat tw= new DecimalFormat("#.##");
		
		//payable is amount that has to be paid by client to the insurer for enjoying particular policy perks until new endorsement, as per previous policy
		double payable=Double.valueOf(tw.format((initial_prem*days_before_endorsement/365)));
		
		//payable is amount that has to be paid by client to the insurer for remaining days as per the endorsement
		double payable_after_endorsement=Double.valueOf(tw.format(new_prem*days_remaining/365));
		
		int misc_days=duration-365;
		
		//misc is change extra premium if policy is not of 365 days
		double misc=0;
		
		misc=(initial_prem/365)*misc_days;
		
		//modi_prem is the amount generated if the policy is not of 365 days
		double modi_prem=0;
		modi_prem=initial_prem+misc;
		
		//change is the difference between previous policy and new policy that will be reflecting premiumSummary page
		double change=Double.valueOf(tw.format((payable+payable_after_endorsement-modi_prem)));
		
		System.out.println(change);
		JFrame parent = new JFrame();
		JOptionPane.showMessageDialog(parent, "Transaction Detail Net Net premium is : "+change);
		return change;
	}

	public static void main(String[] args) throws ScriptException, NoSuchMethodException {
		
		 JFrame frame = new JFrame();
		 Object result = JOptionPane.showInputDialog(frame, "Enter MTA Duration:");
		 Object result1 = JOptionPane.showInputDialog(frame, "Enter Initial Premium:");
		 Object result2 = JOptionPane.showInputDialog(frame, "Enter New Premium:");
		 Object result3 = JOptionPane.showInputDialog(frame, "Enter days before Endorsement:");
		 Object result4 = JOptionPane.showInputDialog(frame, "Enter days remaining:");
		 
		 System.out.println(result);
		
		 checkprem(Integer.parseInt(result.toString()),Double.parseDouble(result1.toString()),Double.parseDouble(result2.toString()),Integer.parseInt(result3.toString()),Integer.parseInt(result4.toString()));
		 checkprem(255,1924.00,0.0,100,155);
		 checkprem(255,35900.93,35948.30,100,155);

	}

}
